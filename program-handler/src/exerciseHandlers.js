// @ts-check

import fs from 'fs';
import fsp from 'fs/promises';
import fse from 'fs-extra';
import path from 'path';
import readdirp from 'readdirp';
import yaml from 'js-yaml';
import _ from 'lodash';
import util from 'util';
import { exec } from 'child_process';
import { Marp } from '@marp-team/marp-core';

const execAsync = util.promisify(exec);

const removeItems = (itemsPaths) => Promise.all(
  itemsPaths.map((itemPath) => fse.remove(itemPath)),
);

const cleanItems = (itemsPaths) => Promise.all(
  itemsPaths.map((itemPath) => fsp.truncate(itemPath)),
);

export const processAction = async (exercisePath) => {
  const dataPath = path.join(exercisePath, '__data__');
  const actionsPath = path.join(dataPath, 'actions.yml');

  if (!fs.existsSync(actionsPath)) {
    return;
  }

  const actionsContent = (await fsp.readFile(actionsPath)).toString();
  const actionsSpec = yaml.load(actionsContent);

  if (!_.isPlainObject(actionsSpec)) {
    throw new Error(`Incorrect contents of the ${actionsPath}`);
  }

  const methods = Object.keys(actionsSpec);

  const methodPromises = methods.map(async (method) => {
    const itemPaths = actionsSpec[method].map(
      (relativePath) => path.resolve(dataPath, relativePath),
    );
    const promises = itemPaths.map((p) => fsp.access(p));
    await Promise.all(promises);

    switch (method) {
      case 'remove':
        await removeItems(itemPaths);
        break;
      case 'clean':
        await cleanItems(itemPaths);
        break;
      default:
        throw new Error(`Unexpected method ${method} in ${actionsPath}`);
    }
  });
  await Promise.all(methodPromises);
};

const removeSolution = (content) => {
  const rewriteRegexp = /(?<begin>^[^\n]*?BEGIN.*?$\s*)(?<content>.+?)(?<end>^[^\n]*?END.*?$)/msug;
  const rewrited = content.search(rewriteRegexp) !== -1;
  const processedContent = rewrited
    ? content.replaceAll(rewriteRegexp, '$1\n$3')
    : content;
  return { rewrited, processedContent };
};

export const processMarkers = async (exercisePath) => {
  const isNotReadmeFile = (fileInfo) => (
    fileInfo.basename.toLowerCase() !== 'readme.md'
  );

  const filesInfo = await readdirp
    .promise(exercisePath, { fileFilter: isNotReadmeFile });
  const filePaths = filesInfo.map((file) => file.fullPath);

  const promises = filePaths.map(async (filePath) => {
    const content = await fsp.readFile(filePath, 'utf-8');
    const { rewrited, processedContent } = removeSolution(content);
    if (rewrited) {
      await fsp.writeFile(filePath, processedContent);
    }
  });
  await Promise.all(promises);
};

export const removeIgnoredFiles = async (exercisePath) => {
  await fse.remove(path.join(exercisePath, '__data__'));
};

export const generatePresentation = async (exercisePath) => {
  const dataPath = path.join(exercisePath, '__data__');
  const presentationPath = path.join(dataPath, 'presentation.md');

  if (!fs.existsSync(presentationPath)) {
    return;
  }

  // NOTE: в pdf либа marp не умеет конвертировать, только marp-cli.
  // https://marpit.marp.app/?id=how-to-use
  // Найти решение с конфертацией без cli
  await execAsync(
    `marp --allow-local-files --pdf -I ${dataPath} -o ${exercisePath}`,
  );
};

export const convertReadmeToHtml = async (exercisePath) => {
  const markdownContent = await fsp.readFile(
    path.join(exercisePath, 'README.md'),
    'utf-8',
  );

  const marp = new Marp();
  // NOTE: Высота задана с запасом.
  // При необходимости можно будет увеличить
  marp.themeSet.default = marp.themeSet.add(`
    /* @theme my-first-theme */

    section {
      width: 1280px;
      height: 5000px;
    }

    @import 'gaia'
  `);

  const { html, css } = marp.render(markdownContent);
  const htmlContent = `
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
      </head>
      <body>
        <style>${css}</style>
        ${html}
      </body>
    </html>
  `;

  await fsp.writeFile(
    path.join(exercisePath, 'README.html'),
    htmlContent,
  );
};
