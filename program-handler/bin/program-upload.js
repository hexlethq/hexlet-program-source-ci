#!/usr/bin/env node

import { uploadProgram } from '../index.js';

const distPath = process.env.MOUNT_PATH;
const projectName = process.env.CI_PROJECT_NAME;
const accessKeyId = process.env.DO_SPACES_KEY;
const secretAccessKey = process.env.DO_SPACES_SECRET;
const spaceEndpoint = 'fra1.digitaloceanspaces.com';
const spaceName = 'hexlet-programs';
const spacePath = '';
const permissionLevel = 'public-read';

uploadProgram({
  distPath,
  projectName,
  accessKeyId,
  secretAccessKey,
  spaceEndpoint,
  spaceName,
  spacePath,
  permissionLevel,
});
