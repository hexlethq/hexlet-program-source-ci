check: lint test

ci-check:
	docker-compose -f docker-compose.yml build
	docker-compose -f docker-compose.yml run exercises make setup
	docker-compose -f docker-compose.yml run -v `pwd`/exercises:/app/exercises exercises make check

compose-setup: compose-build compose-install

compose:
	docker-compose up

compose-down:
	docker-compose down -v --remove-orphans

compose-build:
	docker-compose build

compose-install:
	docker-compose run exercises make setup

compose-bash:
	docker-compose run exercises bash

compose-test:
	docker-compose run exercises make test

compose-lint:
	docker-compose run exercises make lint

setup:
	make -C program-handler install

test:
	make -C program-handler test

test-without-solutions:
	@(for i in $$(find exercises/* -maxdepth 1 -type f -name Makefile); do ! make test -C $$(dirname $$i) || exit 1; done)

check-current:
	make test-current EXERCISE=${EXERCISE}
	make lint-current EXERCISE=${EXERCISE}

test-current:
	echo exercise ${EXERCISE} tests passed

lint-current:
	echo exercise ${EXERCISE} codestyle check passed

lint:
	make -C program-handler lint

release:
	git push -f origin main:release
